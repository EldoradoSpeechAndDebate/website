Team Policies
-------------

.. warning::

   This page is still under construction.

Membership
==========

Membership in our team is open to all students who wish to participate.

Eligibility
^^^^^^^^^^^

Student eligibility is the same as defined by Eldorado High School, namely
having a minimum GPA of 2.0 and no 'F's in the prior semester.

Requirements of Members
^^^^^^^^^^^^^^^^^^^^^^^

Team members are required to:

* Behave in a kind, respectful, and professional manner at all times.  Foul
  language/humor and other inappropriate behaviors will not be tolerated, and
  may result in a temporary suspension from competition.
* Thoroughly prepare for every tournament where they compete.
* Attend all practices that they are able.
* Spend time outside of practices preparing for their
  events.  Improvement only comes through hard work and time.
* Compete in at least three different events over the course of the year.
  Improving your skills in one event will indirectly improve your skills in all
  events.
* Compete in at least two of the three (Speech, Debate, and Congress)  The more well-rounded you are as a
  competitor, the better. 


Lettering
^^^^^^^^^

In order to earn a Varsity letter for Speech and Debate, students must:

* compete in at least two-thirds of the tournaments
* attend at least two-thirds of the practices
* earn at least 75 `merit points <https://www.speechanddebate.org/honor-society/>`_
* compete in at least two of the three (Speech, Debate, and Congress) 

.. note::

    If our practice times do not work for a student’s schedule, alternate
    arrangements can be made at the discretion of the coach(es) and sponsor(s).

Officers
========

If a team member is a senior or has at least two years of experience with
speech and debate, they can put themselves forward to be a *captain*.
Recognizing them as such will be at the discretion of the coach(es) and
sponsor(s).  The senior captains shall be responsible for:

* the overall health and welfare of the team
* making sure students are getting the help they need
* coordinating the collection/dissemination of research
* coordinating team dinners after competition

Practices
=========
Practices will be held online on Tuesdays and Thursdays from 2:30-4:00pm using Google Classroom and Discord.  

Practices will begin with a brief time of announcements, followed generally by
students working on their events.  Students may wind up working individually or
in small groups.  They may be conducting research, writing speeches, watching
performances, giving feedback, etc.  The coach and assistant coach(es) will
float around between the students, giving help and guidance as needed.
Occasionally whole practices will be set aside for mock events or other
activities, e.g., fundraising or advertising efforts.

Schedule
^^^^^^^^

.. warning::

   The times of practices has not yet been confirmed for the 2020/2021
   season.

Cost
====

Dues
^^^^

In order to participate in our team, dues of $100 per person are required to
be paid before the first tournament of each semester.  These dues support the
team in a variety of ways, including:

* allowing us to compete at tournaments
* providing resources to the team
* helping us host a tournament
* helping us travel to tournaments out of district
* providing for our end-of-the-year banquet
* etc.

Drop Fees
^^^^^^^^^

In the event that a teammate registers to compete at a tournament, and then
later has to drop out, there may be an associated drop fee.  These drop fees
are assessed by the individual tournament directors, and are usually around $10
per event.  Whenever our team needs to pay drop fees to a tournament, the
responsible student(s) will need to reimburse the team for the drop fees.

Attending a Tournament
======================
.. warning::
    Due to COVID-19, our tournaments for the 20-21 season will be held online. More information to come. 

For a student to compete at a tournament, we will need to know and have some
paperwork on hand *two weeks in advance*.  Specifically, we need:

* What event(s) you'll be competing in.
* If competing with a partner, who your partner is.
* How you are getting to the tournament (driving yourself, riding with parent,
  riding with other student).
* If you will be driving yourself, we'll need
  `this driving form <https://drive.google.com/file/d/1k1jl7Yzxsk3x_t2IGoPvEfr1ZPgMdB56/view?usp=sharing>`_
  filled out and on file, along with a copy of your driver's license and
  insurance card.
* `This permission slip
  <https://drive.google.com/file/d/1OiZlnCgQHrtYiaLUojywq-1CZe2n68KJ/view?usp=sharing>`_
  signed by a parent/guardian allowing you to participate.
