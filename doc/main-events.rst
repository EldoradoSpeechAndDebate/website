Main Events
-----------

These events are those typically found at tournaments.

.. toctree::
   :titlesonly:

   policy-debate
   public-forum-debate
   lincoln-douglas-debate
   congressional-debate
   big-questions-debate
   original-oratory
   informative-speaking
   program-oral-interpretation
   humorous-interpretation
   dramatic-interpretation
   duo-interpretation
   extemporaneous-speaking
