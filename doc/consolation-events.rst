Consolation Events
------------------

These events may be available at some New Mexico tournaments, but not all.

.. toctree::
   :titlesonly:

   impromptu
   storytelling
   declamation
