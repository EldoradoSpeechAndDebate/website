Policy Debate
-------------
.. epigraph::
    Main Event

.. contents::
   :local:

In A Nutshell
================
Policy debate is an event where students compete with a partner to debate a policy that should be enacted by the United States government. The topic is kept the same for the whole school year so students have time to go in-depth in their analysis. The affirmative team proposes a plan or policy, and the negation debates why the policy should be rejected. 

Event Description
=================

.. image:: ../images/Policy-Debate-1.png
.. image:: ../images/Policy-Debate-2.png
.. image:: ../images/Policy-Debate-3.png
.. image:: ../images/Policy-Debate-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Policy-Debate.png

Resources
=========

Resolution
^^^^^^^^^^

.. note::

   See the `NSDA Topics page <https://www.speechanddebate.org/topics/>`_
   for up-to-date information.

Resolved:  The United States federal government should enact substantial
criminal justice reform in the United States in one or more of the following:
forensic science, policing, sentencing.

Google Drive
^^^^^^^^^^^^

We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1tVbYNM81lMAe92I7M5LrHETb7zwC0hcg?usp=sharing>`_
to collect all of our research, cases, etc.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/HXGNNhOxrBw" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Policy-Debate.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Policy-Debate-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Policy-Debate-Comments.png
