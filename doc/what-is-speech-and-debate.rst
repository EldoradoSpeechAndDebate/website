What is Speech and Debate?
--------------------------

It's an academic activity in which students compete in a variety of events.
Students build their public speaking skills, to be sure, but they also develop
in

* their ability to think critically,
* their ability to see and defend multiple sides of an issue,
* their ability to reason from a philosophical or evidentiary standpoint,
* their ability to persuade others and motivate them to action,
* their ability to act and entertain,
* their ability to have informed discussions on current events,

and the list goes on.  Speech and Debate is viewed very highly by college
admissions officers, but you can see from the list above that it’s an activity
that provides students with invaluable life skills that will go well beyond
their time in formal education.  Below you'll find links to all the events with
descriptions of what they are and what to expect when competing in them.

:download:`Download the complete National Speech and Debate Association manual <manual.pdf>`.

.. include:: main-events.rst

.. include:: supplemental-events.rst

.. include:: consolation-events.rst
