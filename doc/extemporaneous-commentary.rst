Extemporaneous Commentary
-------------------------
.. epigraph::
    Supplemental Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Extemporaneous-Commentary-1.png
.. image:: ../images/Extemporaneous-Commentary-2.png
.. image:: ../images/Extemporaneous-Commentary-3.png
.. image:: ../images/Extemporaneous-Commentary-4.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/Zm7oUydkoNM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
