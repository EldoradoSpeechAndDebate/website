How to Write a Speech
---------------------

.. warning::

   This page is still under construction.

.. contents::
   :local:

INSERT INTRO HERE

Topic Selection
===============

Before you sit down to write a speech, you first need to figure out what you'll
be speaking about.  Pick something you're passionate about.  If you're going to
be speaking to a room full of people, they'll need to feel your energy and
enthusiasm for your topic.  When writing an
:doc:`Original Oratory <original-oratory>`, consider questions like:

* What's something wrong with the world that you desperately want to fix?
* What's something commendable that you want to encourage others to do?
* If you only have a few minutes left to live, what's one message that you
  absolutely must pass on before you go?

When writing for :doc:`Informative Speaking <informative-speaking>`, consider
questions like:

* What's something really neat that you want to share with others?
* What's something you can't believe you never knew before?
* How can I improve someone's life by cluing them in to something they've been
  oblivious to?

In some events, you won't have the luxury of selecting your topic from out of
thin air.  In :doc:`Congressional Debate <congressional-debate>`, you'll be
speaking for or against a particular bill that you probably didn't write.  In
the four other debate events, you'll be either affirming or negating a
resolution that was set before the competition season began.  In these
instances, though, you still get to bring your own perspective to the
discussion at hand, so while you don't have complete and total freedom in
deciding what you'll talk about, make sure what you decide to focus on is
something you can fully support with zeal.

Structure
=========

Once you have your subject matter clear in mind, it's time to start putting
words to the page.  In terms of its structure, a speech will have three main
components: the *introduction*, the *body*, and then the *conclusion*.  Each of
these sections, in turn, will also consist of certain elements.  Let's walk
through each of them in detail.

Introduction
^^^^^^^^^^^^

The introduction is just that: a means of introducing yourself and your
audience to your topic.  It's what sets the stage for the next few minutes, and
lets your listeners know why they really want to pay attention to what you have
to say.  Time is precious, and your introduction needs to convey to your
audience that you deeply value the time they're about to lend you.  How do you
do that?

A first component is what's known as an *attention-getting device*, or AGD.  This is what grabs your listeners' attention right off the bat.

vehicle

`thesis statement <https://en.wikipedia.org/wiki/Thesis_statement>`_

outline

why should the audience care?

Body
^^^^

Conclusion
^^^^^^^^^^

Tone
====
