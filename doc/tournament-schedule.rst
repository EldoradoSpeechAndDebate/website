Tournament Schedule
-------------------


.. note::

    The table below is subject to change.

Sept 19: Congressional Debate Scrimmage hosted virtually by East Mountain High

Oct 16-17: Tournament virtually hosted by EMHS  

Oct 30-31: Full Speech and Debate Tournament virtually hosted by Cottonwood Classical 

Nov 6-7: Tournament virtually hosted by UNM  

Nov 13-14: Tournament virtually hosted by Cleveland/NMSDA ??? 

Dec 4-5: Tournament virtually hosted by Eldorado

Jan 15-16: Tournament virtually hosted by La Cueva 

Jan 29-30: Tournament virtually hosted by Cottonwood Classical 

Feb 5-7: Tournament of Champions (TOC) Bid tournament virtually hosted by New Mexico Speech and Debate Foundation (NMSDF)
Feb 25-27: State Speech and Debate tournament hosted virtually by NMSDA

March 5-6: State Congress tournament virtually hosted by NMSDA 

March 18-20: National Qualifying Tournament (Nat Quals) - high school only - virtually hosted by NMSDA
