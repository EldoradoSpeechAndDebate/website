Lincoln-Douglas Debate
----------------------
.. epigraph::
    Main Event

.. contents::
   :local:

   In a Nutshell
   ==============
   In Lincoln Douglas Debate (also called LD) individuals debate one-on-one on topics that change every other month. Based on the   historical debates of Abraham Lincoln and Stephen Douglas, this debate centers around morality, justice and what "ought" to happen. 

Event Description
=================

.. image:: ../images/Lincoln-Douglas-Debate-1.png
.. image:: ../images/Lincoln-Douglas-Debate-2.png
.. image:: ../images/Lincoln-Douglas-Debate-3.png
.. image:: ../images/Lincoln-Douglas-Debate-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Lincoln-Douglas-Debate.png

Resources
=========

Resolutions
^^^^^^^^^^^

.. note::

   See the `NSDA Topics page <https://www.speechanddebate.org/topics/>`_
   for up-to-date information.

Novice
~~~~~~

Resolved: Civil disobedience in a democracy is morally justified.

Varsity
~~~~~~~

.. note::

   The resolutions for the 2020-2021 season have not been released yet.

Google Drive
^^^^^^^^^^^^

We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1Cvq9oGbtjjjzzpEonBfd4OoSywpTXOvE?usp=sharing>`_
to collect all of our research, cases, etc.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/2knh6K-1K4M" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/LFFOLNdSOWE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Lincoln-Douglas-Debate.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Lincoln-Douglas-Debate-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Lincoln-Douglas-Debate-Comments.png
