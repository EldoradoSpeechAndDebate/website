Duo Interpretation
------------------
.. epigraph::
    Main Event

.. contents::
   :local:
In A Nutshell
================
Duo Interpretation is similar to humorous or dramatic interpretation except it is performed with a partner. Students perform a 10-minute interpretation of a story or play. 

Event Description
=================

.. image:: ../images/Duo-Interpretation-1.png
.. image:: ../images/Duo-Interpretation-2.png
.. image:: ../images/Duo-Interpretation-3.png
.. image:: ../images/Duo-Interpretation-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Duo-Interpretation.png

Resources
=========

Scripts
^^^^^^^

See `our scripts repository <https://drive.google.com/drive/folders/1e4tCEEr-PhK_BJEfAP8uJEHfKj3TkwOL?usp=sharing>`_ in our Google Drive.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/obxsMkx51pY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/cH-iKFDast8" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/IsZaSFb7dS4" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/BBWqdu7WufE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Interpretation.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Interp-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Interp-Comments.png
