Eldorado Speech & Debate
------------------------

Welcome to the Eldorado Speech and Debate homepage. This is where you'll get a crash course on all things Speech and Debate. 

Please read through our :doc:`Team Policies <team-policies>` page to get a feel for how we operate.


If you're brand new to speech and debate, :doc:`read this brief introduction <what-is-speech-and-debate>` and click
through the links to find out about all the different events you can compete
in.  

We can be reached by email at abq.eldorado.speech.and.debate@gmail.com 
Or the team's coach Kathryn Sokolowski can be reached by kathryns221@gmail.com or kathryn.sokolowski@aps.edu

Thanks for joining the team.  We're excited to have you!

.. toctree::
   :titlesonly:

   what-is-speech-and-debate
   team-policies
   what-is-slack
   onboarding-checklist
   tournament-schedule
   training
   judging
   running-a-tournament

