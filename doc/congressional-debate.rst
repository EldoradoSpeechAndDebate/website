Congressional Debate
--------------------
.. epigraph::
    Main Event

.. contents::
   :local:
In a Nutshell
================
Students act as United States senators and representatives to debate and pass legislation in a chamber of other delegates. Students may also write and introduce their own legislation. 


Event Description
=================

.. image:: ../images/Congressional-Debate-1.png
.. image:: ../images/Congressional-Debate-2.png
.. image:: ../images/Congressional-Debate-3.png
.. image:: ../images/Congressional-Debate-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Congressional-Debate.png

Resources
=========

Google Drive
^^^^^^^^^^^^

We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1dPxA8eVexvYM_8Ka0_byMZFGq8iUCy4m?usp=sharing>`_
to collect all of our research, bills, etc.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/HQ07Dmc1L0g" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Congressional-Debate-Blank-0.png
.. image:: ../images/Sample-Ballot-Congressional-Debate-Blank-1.png
