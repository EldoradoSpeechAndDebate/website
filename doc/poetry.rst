Poetry
------
.. epigraph::
    Supplemental Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Poetry-1.png
.. image:: ../images/Poetry-2.png
.. image:: ../images/Poetry-3.png
.. image:: ../images/Poetry-4.png
.. image:: ../images/Poetry-5.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/2QMH_yRH3lc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Prose-Poetry-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Prose-Poetry-Comments.png
