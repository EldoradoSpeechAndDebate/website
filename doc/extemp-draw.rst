Extemp Draw
-----------

Here's the recommended procedure for running the extemp draw at a tournament.

.. contents::
   :local:

Initial Setup
=============

Supplies Needed
^^^^^^^^^^^^^^^

* A number of envelopes with questions in them, one for each round of
  competition for both USX and IX.
* A reliable means of keeping time (watch, clock, phone, etc.)
* Blank paper.
* Writing utensils.

Get Ready
^^^^^^^^^

Pick a table in a prominent location in the extemp draw room from which to run
the draw.  Write "USX" on one sheet of paper and "IX" on another, then place
these papers on the table to indicate which half of the table is dedicated to
which event.

For Each Round
==============

Set Out the Questions
^^^^^^^^^^^^^^^^^^^^^

Before a round starts, grab the envelopes for the round and empty the questions
for USX and IX onto the appropriate halves of the table.  Make sure all the
slips of paper are turned face down, and arrange them in such a way that it
will be relatively easy for multiple people to draw at a time.

Have Students Draw
^^^^^^^^^^^^^^^^^^

At the appropriate time (see the `Schedule`_ below), call out the appropriate
codes for the students to come and draw.  Each student should draw three slips
of paper, pick one, and return the other two to the table.  Make sure students
don't get in each other's way if the tournament is running multiples sections
of extemp per round.

Wait Between Draws
^^^^^^^^^^^^^^^^^^

From the time you first call a batch of students up to draw to the next time
you call up a batch of students, there is a seven-minute wait time.

Dismiss Students to their Rooms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Five minutes before a batch of students is to present before their judges,
twenty-five minutes after they drew their topics, dismiss them to head to their
competition rooms.  Four minutes later, give a one-minute warning to any
students who have not yet left the extemp draw room.

Schedule
========

The schedule for an extemp draw can be a little complicated, just because there
are so many things to keep track of.  The table below is an example of how a
round would work.  At the extemp draw start time, start a stopwatch and call
the first set of speakers up to draw.  Then let the stopwatch run and keep an
eye on it so you know when to call up the next speakers, dismiss them to their
rooms, etc.  The time in the table below is the number of minutes that have
elapsed since the extemp draw started.

+------+-----------------------------------+
| Time | Task                              |
+======+===================================+
| 0    | Call 1st speakers to draw         |
+------+-----------------------------------+
| 7    | Call 2nd speakers to draw         |
+------+-----------------------------------+
| 14   | Call 3rd speakers to draw         |
+------+-----------------------------------+
| 15   | *1st speakers 15 minute reminder* |
+------+-----------------------------------+
| 21   | Call 4th speakers to draw         |
+------+-----------------------------------+
| 22   | *2nd speakers 15 minute reminder* |
+------+-----------------------------------+
| 25   | **Dismiss 1st speakers to rooms** |
+------+-----------------------------------+
| 28   | Call 5th speakers to draw         |
+------+-----------------------------------+
| 29   | *Remind 1st speakers to leave*    |
+------+-----------------------------------+
| 29   | *3rd speakers 15 minute reminder* |
+------+-----------------------------------+
| 32   | **Dismiss 2nd speakers to rooms** |
+------+-----------------------------------+
| 35   | Call 6th speakers to draw         |
+------+-----------------------------------+
| 36   | *Remind 2nd speakers to leave*    |
+------+-----------------------------------+
| 36   | *4th speakers 15 minute reminder* |
+------+-----------------------------------+
| 39   | **Dismiss 3rd speakers to rooms** |
+------+-----------------------------------+
| 42   | Call 7th speakers to draw         |
+------+-----------------------------------+
| 43   | *Remind 3rd speakers to leave*    |
+------+-----------------------------------+
| 43   | *5th speakers 15 minute reminder* |
+------+-----------------------------------+
| 46   | **Dismiss 4th speakers to rooms** |
+------+-----------------------------------+
| 49   | Call 8th speakers to draw         |
+------+-----------------------------------+
| 50   | *Remind 4th speakers to leave*    |
+------+-----------------------------------+
| 50   | *6th speakers 15 minute reminder* |
+------+-----------------------------------+
| 53   | **Dismiss 5th speakers to rooms** |
+------+-----------------------------------+
| 57   | *Remind 5th speakers to leave*    |
+------+-----------------------------------+
| 57   | *7th speakers 15 minute reminder* |
+------+-----------------------------------+
| 60   | **Dismiss 6th speakers to rooms** |
+------+-----------------------------------+
| 64   | *Remind 6th speakers to leave*    |
+------+-----------------------------------+
| 64   | *8th speakers 15 minute reminder* |
+------+-----------------------------------+
| 67   | **Dismiss 7th speakers to rooms** |
+------+-----------------------------------+
| 71   | *Remind 7th speakers to leave*    |
+------+-----------------------------------+
| 74   | **Dismiss 8th speakers to rooms** |
+------+-----------------------------------+
| 78   | *Remind 8th speakers to leave*    |
+------+-----------------------------------+
