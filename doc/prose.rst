Prose
-----
.. epigraph::
    Supplemental Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Prose-1.png
.. image:: ../images/Prose-2.png
.. image:: ../images/Prose-3.png
.. image:: ../images/Prose-4.png
.. image:: ../images/Prose-5.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/0sTcrTErtt0" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Prose-Poetry-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Prose-Poetry-Comments.png
