World Schools Debate
--------------------
.. epigraph::
    Main Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/World-Schools-Debate-1.png
.. image:: ../images/World-Schools-Debate-2.png
.. image:: ../images/World-Schools-Debate-3.png
.. image:: ../images/World-Schools-Debate-4.png
.. image:: ../images/World-Schools-Debate-5.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/4HUFM3JZaLQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-World-Schools-Debate-Blank-0.png
.. image:: ../images/Sample-Ballot-World-Schools-Debate-Blank-1.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-World-Schools-Debate-Comments-0.png
.. image:: ../images/Sample-Ballot-World-Schools-Debate-Comments-1.png
