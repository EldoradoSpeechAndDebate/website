Expository Speaking
-------------------
.. epigraph::
    Supplemental Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Expository-Speaking-1.png
.. image:: ../images/Expository-Speaking-2.png
.. image:: ../images/Expository-Speaking-3.png
.. image:: ../images/Expository-Speaking-4.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/KXq64WXjkqg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
