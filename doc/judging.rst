Judging
-------

If you will be judging for us at some point in the year, thank you for choosing
to help out.  This activity is one that simply cannot happen without the
support of our community, so we deeply value your contributions.  Check out the
pages for our :doc:`main <main-events>`,
:doc:`supplemental <supplemental-events>`,
and :doc:`consolation <consolation-events>` events, and pay particular
attention to the **Judging** section of each page.  There you'll find
information on what to look for when judging that particular event, along with
sample ballots, both blank and with comments filled out as an example.  If you
have any questions, just let us know.

Additionally, you can check out this very thorough
`online course on adjudicating speech and debate <https://www.nfhslearn.com/courses/61139/adjudicating-speech-and-debate>`_.
Beyond that, the
`National Speech and Debate Association <https://www.speechanddebate.org>`_
has also put together
`a number of instructional videos <https://www.speechanddebate.org/judge-training/>`_
to help you get up-to-speed with judging speech and debate.

.. image:: ../images/How-To-Be-A-Great-Volunteer-Judge.png
