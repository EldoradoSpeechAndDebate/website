Extemporaneous Speaking
-----------------------
.. epigraph::
    Main Event

.. contents::
   :local:
In A Nutshell
================
Extemporaneous speaking (also called extemp) has two categories, international extemp, and domestic or united states extemp. Students will draw a question prior to speaking and will have 30 minutes to write and prepare their speech on the topic chosen. These are often questions that ask students to form an argument about a current event. 

Event Description
=================

.. image:: ../images/Extemporaneous-Speaking-1.png
.. image:: ../images/Extemporaneous-Speaking-2.png
.. image:: ../images/Extemporaneous-Speaking-3.png
.. image:: ../images/United-States-Extemporaneous-Speaking-1.png
.. image:: ../images/International-Extemporaneous-Speaking-1.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-United-States-Extemporaneous-Speaking.png
.. image:: ../images/What-to-Expect-Competing-in-International-Extemporaneous-Speaking.png

Resources
=========

* We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1O5iLFlFv_akFJN-vJbdkSzI-IhPVjHcM?usp=sharing>`_ for collecting research.
* We also recommend competitors purchase a subscription to `Extemp Genie <https://extempgenie.com/>`_.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/FLSxCk0_XW4" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/TI1aQq4WBsk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/IwS9GNCcb_8" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Extemporaneous-Speaking.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Extemp-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Extemp-Comments.png
