Big Questions Debate
--------------------
.. epigraph::
    Main Event

.. contents::
   :local:

   In a Nutshell
   ==============
   Big Questions debate is the NSDA's newest debate style. Students can compete individually or in pairs to debate a philosophically based question. It is designed to open student's minds and encourage them to engage in life discussion that may not align with their previously held beliefs.

Event Description
=================

Big Questions is designed to enhance students’ current debate experiences, opening their minds and encouraging them to engage in life discussion that may not align with their previously held beliefs. Whether or not students change their opinion, the rich experience of this debate event will advance their knowledge, comfort, and interest in learning more about the subject matter.

.. image:: ../images/Big-Questions-Format-Manual-1.png
.. image:: ../images/Big-Questions-Format-Manual-2.png
.. image:: ../images/Big-Questions-Format-Manual-3.png
.. image:: ../images/Big-Questions-Format-Manual-4.png
.. image:: ../images/Big-Questions-Format-Manual-5.png

Resources
=========

Resolution
^^^^^^^^^^

.. note::

   See the `NSDA Topics page <https://www.speechanddebate.org/topics/>`_
   for up-to-date information.

.. note::

   The resolution for the 2020-2021 season has not been released yet.

Google Drive
^^^^^^^^^^^^

We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1CVwineTTHAUmqGCkDBEZ8dgnSeTsuSqS?usp=sharing>`_
to collect all of our research, cases, etc.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/e9w7bhapHIE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
