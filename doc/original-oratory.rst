Original Oratory
----------------
.. epigraph::
    Main Event

.. contents::
   :local:

In a Nutshell
================
Students write, memorize, and deliver a 10-minute speech on a topic that is important to the speaker. These speeches can be persuasive or informative. Students aim to craft an argument and deliver it in a meaningful or impactful way. 

Event Description
=================

.. image:: ../images/Original-Oratory-1.png
.. image:: ../images/Original-Oratory-2.png
.. image:: ../images/Original-Oratory-3.png
.. image:: ../images/Original-Oratory-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Original-Oratory.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/iYp3QqXS4ic" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/Qb8rmsMhFCo" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/4lSFjmcld9w" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Original-Oratory.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Oratory-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Oratory-Comments.png
