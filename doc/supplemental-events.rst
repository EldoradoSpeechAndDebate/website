Supplemental Events
-------------------

Unfortunately these events generally are not found at tournaments in New
Mexico, though they are found at larger regional tournaments and especially the
national tournament.

.. toctree::
   :titlesonly:

   extemporaneous-commentary
   prose
   poetry
   extemporaneous-debate
   expository-speaking
