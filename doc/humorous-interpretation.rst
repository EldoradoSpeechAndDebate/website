Humorous Interpretation
-----------------------
.. epigraph::
    Main Event

.. contents::
   :local:
In a Nutshell
================
The goal of humourous interpretation is to make the audience laugh. Students will choose and "cut" a published piece of literature or a play to create a 10-minute performance of the story in a humourous way.


Event Description
=================

.. image:: ../images/Humorous-Interpretation-1.png
.. image:: ../images/Humorous-Interpretation-2.png
.. image:: ../images/Humorous-Interpretation-3.png
.. image:: ../images/Humorous-Interpretation-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Humorous-Interpretation.png

Resources
=========

Scripts
^^^^^^^

See `our scripts repository <https://drive.google.com/drive/folders/1WW6SFQMz1xsPXeHLXaRoG8bM8PYLQs8u?usp=sharing>`_ in our Google Drive.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/4wyIH3YcuYk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/sg4uRwGmK4k" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/-5l6M2sOVFA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/tQ_nRFuYASM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Interpretation.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Interp-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Interp-Comments.png
