Public Forum Debate
-------------------
.. epigraph::
    Main Event

.. contents::
   :local:

In a Nutshell
================
Public Forum Debate (also called Puff or PF) is a partner debate which focuses on debating current events. The topic changes monthly and cover a wide range of topics. Upon starting the round, teams conduct a coin toss to determine which team will be affirmative or the negation. 


Event Description
=================

.. image:: ../images/Public-Forum-Debate-1.png
.. image:: ../images/Public-Forum-Debate-2.png
.. image:: ../images/Public-Forum-Debate-3.png
.. image:: ../images/Public-Forum-Debate-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Public-Forum-Debate.png

Resources
=========

Resolutions
^^^^^^^^^^^

.. note::

   See the `NSDA Topics page <https://www.speechanddebate.org/topics/>`_
   for up-to-date information.

.. note::

   The resolutions for the 2020-2021 season have not been released yet.

Google Drive
^^^^^^^^^^^^

We use a `shared Google Drive folder <https://drive.google.com/drive/folders/1JZZT53kn9Al_NXQoCeHn4tvcheQ7MrZ0?usp=sharing>`_
to collect all of our research, cases, etc.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/MUnyLbeu7qU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Public-Forum-Debate.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Public-Forum-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Public-Forum-Debate-Comments.png
