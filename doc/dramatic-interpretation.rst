Dramatic Interpretation
-----------------------
.. epigraph::
    Main Event

.. contents::
   :local:

In A Nutshell
================
The goal of Dramatic Interpretation is to make the audience cry. Students choose and cut a story or play to create a 10-minute performance that is dramatic in nature. 

Event Description
=================

.. image:: ../images/Dramatic-Interpretation-1.png
.. image:: ../images/Dramatic-Interpretation-2.png
.. image:: ../images/Dramatic-Interpretation-3.png
.. image:: ../images/Dramatic-Interpretation-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Dramatic-Interpretation.png

Resources
=========

Scripts
^^^^^^^

See `our scripts repository <https://drive.google.com/drive/folders/1B27HEYIuWs2hUiRh15BHKXaZ6cVU30jj?usp=sharing>`_ in our Google Drive.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/DGh4P3-HFHg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/wNzaq6loj64" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Interpretation.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Interp-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Interp-Comments.png
