Program Oral Interpretation
---------------------------
.. epigraph::
    Main Event

.. contents::
   :local:

In a Nutshell
================
In Program Oral Interpretation (also called Oral Interpretation or POI) students purposefully combine works of poetry, prose, or drama around a central theme or idea. This event is performed with the aid of a small black binder with the chosen literature inside.

Event Description
=================

.. image:: ../images/Program-Oral-Interpretation-1.png
.. image:: ../images/Program-Oral-Interpretation-2.png
.. image:: ../images/Program-Oral-Interpretation-3.png
.. image:: ../images/Program-Oral-Interpretation-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Program-Oral-Interpretation.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/hjeMiRWo0qk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Program-Oral-Interpretation.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-POI-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-POI-Comments.png
