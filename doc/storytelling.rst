Storytelling
------------
.. epigraph::
    Consolation Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Storytelling-1.png
.. image:: ../images/Storytelling-2.png
.. image:: ../images/Storytelling-3.png
.. image:: ../images/Storytelling-4.png
.. image:: ../images/Storytelling-5.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/qtFlsmwxyKs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Storytelling-Blank.png
