Extemporaneous Debate
---------------------
.. epigraph::
    Supplemental Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Extemporaneous-Debate-1.png
.. image:: ../images/Extemporaneous-Debate-2.png
.. image:: ../images/Extemporaneous-Debate-3.png
.. image:: ../images/Extemporaneous-Debate-4.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/I63EiHaYEJc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
