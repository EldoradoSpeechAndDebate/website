Onboarding Checklist
--------------------

Welcome—we're so glad you're here!  To get up-to-speed, please work your way
through the following checklist.

* Make sure you know our homepage:
  https://eldoradospeechanddebate.readthedocs.io.  If you have questions about
  anything, you should be able to find the answers here.
* Read through our :doc:`team policies <team-policies>`.
* Read up on :doc:`Disord <what-is-discord>` 

  * Downlod the Discord app to your desktop/laptop/mobile device.
  * Make sure you enable push notifications so you're able to receive
    announcements, so your coaches can get ahold of you, etc.

* Make sure to add us on Remind by texting @eldodebate to 81010
* Read up on all the different :doc:`events <what-is-speech-and-debate>`.
* Visit the
  `National Speech and Debate Association (NSDA) website <www.speechanddebate.org>`_
  and create a free account if you don't already have one.  This will gain you
  access to resources, allow you to track the points you earn in competition,
  etc.
* Visit `Tabroom <https://tabroom.com>`_ and create a free account if you don't
  already have one.  This is the platform we use to register for competitions. 
* If you will be driving yourself, and possibly others, to tournaments in town,
  submit the following to your coaches:

  * `This driving form <https://drive.google.com/file/d/1k1jl7Yzxsk3x_t2IGoPvEfr1ZPgMdB56/view?usp=sharing>`_.
  * A copy of your driver's license.
  * A copy of your auto insurance card.  Note that you will need to submit a
    new copy any time your insurance is renewed.

If you run into any questions that aren't answered here, or have any comments
to make along the way, let us know over on Discord, or email kathryns221@gmail.com
