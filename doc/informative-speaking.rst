Informative Speaking
--------------------
.. epigraph::
    Main Event

.. contents::
   :local:

In a Nutshell
=================
In informative speaking, students write, memorize and deliver a 10-minute speech about a topic that they wish to inform or educate on. These can be lighthearted or serious in nature. Visual aids such as poster boards or props are permitted and encouraged.

Event Description
=================

.. image:: ../images/Informative-Speaking-1.png
.. image:: ../images/Informative-Speaking-2.png
.. image:: ../images/Informative-Speaking-3.png
.. image:: ../images/Informative-Speaking-4.png

What to Expect
==============

.. image:: ../images/What-to-Expect-Competing-in-Informative-Speaking.png

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/qFvRyaApcLo" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/KXs8lbCFGRQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Introduction
^^^^^^^^^^^^

.. image:: ../images/How-to-Judge-Informative-Speaking.png

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Inform-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Inform-Comments.png
