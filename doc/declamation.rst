Declamation
-----------
.. epigraph::
    Consolation Event

.. contents::
   :local:

Event Description
=================

.. image:: ../images/Declamation-1.png
.. image:: ../images/Declamation-2.png
.. image:: ../images/Declamation-3.png
.. image:: ../images/Declamation-4.png

Resources
=========

Scripts
^^^^^^^

See `our scripts repository <https://drive.google.com/drive/folders/1ChosLP2_vWnWp12SCUwluX1yBw0Raf4m?usp=sharing>`_ in our Google Drive.

Examples
========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/3ZA8t6F-cwU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Judging
=======

Ballots
^^^^^^^

Blank
~~~~~

.. image:: ../images/Sample-Ballot-Declamation-Blank.png

Comments
~~~~~~~~

.. image:: ../images/Sample-Ballot-Declamation-Comments.png
