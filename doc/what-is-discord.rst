What is Discord?
--------------

Discord (discord.com) is originally known as a communication hub for gamers. However, we will be co-opting it for the purposes of communication and practice. Discord allows us to create a private server only for Speech and Debate students to use. In it there are organized channels based on different events, including voice channels and video calls that you can just "hop" into when you're ready.

How Do We Use It?
=================

We will use Discord to host breakout sessions during practice. Because Speech and Debate has so many individualized events, it is difficult to all work together on one Google Meet. Instead, students will break-out into smaller groups depending on the events they are working on. These can be found under "voice channels" in our server. Outside of regular practice time, teammates can get together to chat about speeches, write their cases, or help each other out. 

JOIN OUR SERVER HERE <https://discord.gg/RCWzJwy> 

Channels
========

Discord uses *channels* to keep common threads of information organized.  We have
the following:


* `general: This is for general information sharing relevant to the whole team.  It's where coaches and sponsors will post announcements. 

* `random: This is for any non-speech-and-debate-related conversation.  Saw an awesome movi  last weekend?  Tried a tasty new restaurant?  Whatever you like.

* `help and feedback: Have a logistical question? Need help as we transition to online practices and tournaments? Shoot your message here and someone will help you out. 

Additionally, there are "event channels" these channels are dedicated to specific events where you can share your speeches, get feedback from others. Anything related to individual events will be here. 


